#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

export PATH=$PATH:$HOME/.dotnet/tools:$HOME/.cargo/bin
export EDITOR="vim"
export TERMINAL="termite"
export BROWSER="firefox"

if [[ "$(tty)" = "/dev/tty1" ]]; then
	echo
	echo "Choose environment (nothing for qtile):"
	echo "1. kde"
	echo "2. terminal"
	echo

	read -p 'Option: ' number
	echo

	option="qtile"

	case $number in
		1)
			option="kde";;
		2)
			option="terminal";;
	esac

	if [[ "${option}" != "terminal" ]]; then
		cp ${HOME}/startx_files/${option} ${HOME}/.xinitrc
		exec startx
	fi
fi
